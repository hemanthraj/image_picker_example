import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Image Picker',
      theme: new ThemeData(primarySwatch: Colors.blue, backgroundColor: Colors.white),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> images = <String>[];
  List<File> imageFiles = <File>[];

  void _pickImage(ImageSource source) async {
    if (images.length >= 5) {
      _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text('Images cannot exceed limit of five'),
        ),
      );
      return;
    }
    final File image = await ImagePicker.pickImage(source: source);
    final String imageString = base64.encode(image.readAsBytesSync());
    debugPrint('Image ${images.length + 1}: $imageString');
    images.add(imageString);
    imageFiles.add(image);
    setState(() {});
  }

  List<Widget> _getImages() {
    final List<Widget> children = <Widget>[];

    for (int i = 0; i < images.length; i++) {
      final String data = images[i];
      final Widget image = new AspectRatio(
        aspectRatio: 1.0,
        child: new Image.file(
          imageFiles[i],
          fit: BoxFit.fill,
        ),
      );
      final Widget child = new Card(
        child: new Stack(
          children: <Widget>[
            image,
            new Positioned(
              bottom: 0.0,
              right: 0.0,
              child: new IconButton(
                icon: new Icon(
                  Icons.close,
                  color: Colors.red,
                ),
                onPressed: () {
                  images.removeAt(i);
                  imageFiles.removeAt(i);
                  setState(() {});
                },
              ),
            ),
          ],
        ),
      );
      children.add(child);
    }

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: new AppBar(
        title: const Text('Image Picker'),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new InkResponse(
              onTap: () => _pickImage(ImageSource.camera),
              child: new Container(
                padding: const EdgeInsets.all(16.0),
                child: new Column(
                  children: <Widget>[
                    new Icon(
                      Icons.camera_alt,
                      size: 56.0,
                    ),
                    new Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: new Text(
                        'Take a picture',
                        style: new TextStyle(color: Theme.of(context).primaryColor),
                      ),
                    )
                  ],
                ),
              ),
            ),
            new InkResponse(
              onTap: () => _pickImage(ImageSource.gallery),
              child: new Container(
                padding: const EdgeInsets.all(16.0),
                child: new Column(
                  children: <Widget>[
                    new Icon(
                      Icons.image,
                      size: 56.0,
                    ),
                    new Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: new Text(
                        'Choose from gallery',
                        style: new TextStyle(color: Theme.of(context).primaryColor),
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Expanded(
              child: new Container(
                decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.circular(5.0),
                  border: new Border.all(color: Colors.black),
                ),
                padding: const EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width / 2,
                child: new SingleChildScrollView(
                  child: new Column(
                    children: _getImages(),
                  ),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: new RaisedButton(
                textColor: Theme.of(context).primaryColor,
                onPressed: () => null,
                child: const Text('Save'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
